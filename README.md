## A simple "Hello World!" script using Node.js and Puppeteer (https://pptr.dev)

- Navigates through Wikipedia to the "Hello World!" program wiki and scrapes the title and first paragraph

- Saves the information via fs as a JSON file