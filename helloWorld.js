const pptr = require('puppeteer');
const fs = require('fs');

let helloWorld = async () => {
    const browser = await pptr.launch({headless: false});
    const page = await browser.newPage();
    await page.setViewport({width: 1920, height: 1080});
    await page.goto('http://wikipedia.org')
    await page.type('#searchInput', 'Hello World')
    await page.click('#search-form > fieldset > button')
    
    await page.waitForSelector('#mw-content-text')
    const result = await page.evaluate(() => {
        let title = document.querySelector('#firstHeading').innerText;
        let body = document.querySelector('#mw-content-text > div.mw-content-ltr.mw-parser-output > p:nth-child(5)').innerText;
        return {title, body};
    })

    browser.close();
    return result;
};

helloWorld().then((value) => {
    fs.writeFile('helloWorld.json', JSON.stringify(value), (e) => {
        if (e) console.error(e);
    });
});